import { Routes } from '@angular/router';
import { RegisterComponent } from './user/register/register.component';

export const routes: Routes = [
    { path: 'register', component: RegisterComponent },
];

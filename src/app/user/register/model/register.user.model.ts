export class RegisterUserDTO {
    constructor(
        readonly firstName: string,
        readonly lastName: string,
        readonly email: string,
        readonly birthday: string,
        readonly login: string,
        readonly password: string,
        readonly phone: string, 
        readonly cars: CreateCarDTO[]) {}
}

export class CreateCarDTO {
    constructor(readonly year: number,readonly licensePlate: string,
        readonly model: string, readonly color: string) {}
}
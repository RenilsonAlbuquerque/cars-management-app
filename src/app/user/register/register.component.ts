import { NgFor } from '@angular/common';
import { Component, importProvidersFrom } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { RegisterUserDTO } from './model/register.user.model';
import { HttpClientModule } from '@angular/common/http';
import { UserRegisterService } from '../services/user.register.service';

@Component({
  selector: 'app-register',
  standalone: true,
  templateUrl: './register.component.html',
  styleUrl: './register.component.scss',
  imports: [NgFor, ReactiveFormsModule,HttpClientModule],
  providers: [
    UserRegisterService
  ],
})
export class RegisterComponent {
  public registerForm: FormGroup = this.formBuilder.group({
    firstName: ['', Validators.required],
    lastName: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]],
    birthday: ['', Validators.required],
    login: ['', Validators.required],
    password: ['', [Validators.required, Validators.minLength(8)]],
    phone: ['', Validators.required],
    cars: this.formBuilder.array([])
  });
  
  constructor(private formBuilder: FormBuilder, private userRegisterService: UserRegisterService) { }
     

  onFormSubmit() {
    if (this.registerForm.valid) {
      let result = this.registerForm.value as RegisterUserDTO;
      this.userRegisterService.registerUser(result).subscribe(result => {
        console.log(result);
      });
    }
  }
  get cars(): FormArray {
    
    return this.registerForm.controls['cars'] as FormArray;
  }

  public addNewCar() {
    this.cars.push(this.carFormControlBuilder());
  }

  public deleteCar(carArrayIndex: number) {
    this.cars.removeAt(carArrayIndex);
  }

  private carFormControlBuilder(): FormGroup {
    return this.formBuilder.group({
      year: ['', Validators.required],
      licensePlate: ['', Validators.required],
      model: ['', Validators.required],
      color: ['', Validators.required],
    }) as FormGroup;
  }

}

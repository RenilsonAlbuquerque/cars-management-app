import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RegisterUserDTO } from '../register/model/register.user.model';
import { Observable } from 'rxjs';

@Injectable()
export class UserRegisterService {
    private userApiUrl = 'http://localhost:8080/api/users';
    constructor(private http: HttpClient) {}

    registerUser(user: RegisterUserDTO): Observable<RegisterUserDTO> {
        return this.http.post<RegisterUserDTO>(this.userApiUrl, user);
    }
}